from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import random
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn import cross_validation
import re



def ROC_Curve(model, x_test, y_test, ax = None, n_bootstrap = 10):
    if not ax :
        ax = plt.subplot(111)
    ax.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    
    roc_auc_list = []
    
    for i in range(n_bootstrap) :
        sample_index = [random.randint(0,x_test.shape[0] - 1) for i in xrange(0,x_test.shape[0])]
        x_sample = [x_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        y_sample = [y_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        probas_ = model.predict_proba(x_sample)
        
        fpr, tpr, _ = roc_curve(y_sample, probas_[:, 1])
        
        roc_auc_list += [auc(fpr, tpr)]
        ax.plot(fpr, tpr)
    
    mean_auc =  sum(roc_auc_list)/len(roc_auc_list)
    ax.set_title('ROC curve (area = %0.4f)' % mean_auc)
    
    

def get_AUC(model, x_test, y_test, n_bootstrap = 10):
    
    roc_auc_list = []
    
    for i in range(n_bootstrap) :
        sample_index = [random.randint(0,x_test.shape[0] - 1) for i in xrange(0,x_test.shape[0])]
        x_sample = [x_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        y_sample = [y_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        probas_ = model.predict_proba(x_sample)
        
        fpr, tpr, _ = roc_curve(y_sample, probas_[:, 1])

        roc_auc_list += [auc(fpr, tpr)]
    
    mean_auc =  sum(roc_auc_list)/len(roc_auc_list)
    return(mean_auc)
        
    
def PR_Curve(model, x_test, y_test, ax = None, n_bootstrap = 10):
    if not ax :
        ax = plt.subplot(111)
        
    positive_rate = float(((y_test == 1.0).sum()))/len(y_test)
    
    ax.plot([1, 0], [positive_rate, 1-positive_rate], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Precision Recall curve')

    
    area_list = []
    
    for i in range(n_bootstrap) :
        sample_index = [random.randint(0,x_test.shape[0] - 1) for i in xrange(0,x_test.shape[0])]
        x_sample = [x_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        y_sample = [y_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        probas_ = model.predict_proba(x_sample)
        
        precision, recall, _ = precision_recall_curve(y_sample, probas_[:, 1])
        f1_score = precision*recall
        
        area_list += [average_precision_score(y_sample, probas_[:, 1])]
        ax.plot(recall, precision)
        ax.plot(recall, f1_score)
        
    mean_area =  sum(area_list)/len(area_list)
    ax.set_title('PR curve (area = %0.4f)' % mean_area)
        
def get_PR(model, x_test, y_test,n_bootstrap = 10):

    positive_rate = float(((y_test == 1.0).sum()))/len(y_test)

    area_list = []
    
    for i in range(n_bootstrap) :
        sample_index = [random.randint(0,x_test.shape[0] - 1) for i in xrange(0,x_test.shape[0])]
        x_sample = [x_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        y_sample = [y_test[sample_index[i]] for i in xrange(0,len(sample_index))]
        probas_ = model.predict_proba(x_sample)
        
        precision, recall, _ = precision_recall_curve(y_sample, probas_[:, 1])
        f1_score = precision*recall
        
        area_list += [average_precision_score(y_sample, probas_[:, 1])]
        
    mean_area =  sum(area_list)/len(area_list)
    return(mean_area)  

def plot_dict_bar(D, ax, keys=None) :
    if keys is None:
        keys = D.keys()

    #print keys
    #print [str(key) for key in keys]
    #print D.keys()

    ax.barh(range(len(D)), [D[str(key)] for key in keys], align='center')

    p = 0.003
    ax.set_xlim([min(D.values())*(1-p), max(D.values())*(1+p)])
    ax.grid(True)

    ax.axvline(max([D[str(key)] for key in keys]), color='red')
    if 'nothing removed' in D:
        ax.axvline(max([D['nothing removed'] for key in keys]), color='red')

    tick_keys = []

    for key in keys:
        if key.__class__ == [].__class__:
            tick_key = 'day '
            # handle '_3' for graphs
            for i in range(len(key)):
                if re.match('_\d',key[i]):
                    tick_key += key[i][-1] + ' '


            tick_keys += [tick_key]
        else:
            if re.match('_\d',key):
                tick_keys += ['day %s' % key[-1]]
            else:
                tick_keys += [key]

    plt.yticks(range(len(D)), tick_keys)




def cross_fold_roc(model, X, y, kf=None, ax=None, mean=True, evaluate_train=True, title=''):
    print 'crossfold for %s' % title
    '''
    :param model: model to test
    :param X: data
    :param y: labels
    :param kf: crossfold repartition
    :param ax: if not None, ROC will be plotted on it (must be a list of 2 if evaluate_train)
    :param mean: consider folds separately or consider mean of all folds
    :param evaluate_train: consider evaluation on train set or not
    :return: auc (means or all depending on mean value)
    '''

    def roc_and_auc(probas, labels, ax=None, title=''):
        fpr, tpr, _ = roc_curve(labels, probas)
        if ax is not None:
            auc_value = auc(fpr, tpr)
            ax.plot(fpr, tpr, label = '%s (AUC=%0.4f)' % (title, auc_value))
            ax.legend(loc='lower right')
        return auc_value

    if kf is None:
        kf = cross_validation.KFold(len(X), n_folds=5, shuffle=True, random_state=0)


    if ax is not None:
        if evaluate_train:
            assert ax.__class__ == [].__class__ and len(ax) == 2
            ax_test = ax[0]
            ax_train = ax[1]
        else:
            ax_test = ax




    if mean:
        collect_probas_test = []
        collect_labels_test = []
        if evaluate_train:
            collect_probas_train = []
            collect_labels_train = []
    else:
        collect_auc_test = []
        if evaluate_train:
            collect_auc_train = []

    for i, (train_index, test_index) in enumerate(kf):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        model.fit(X_train, y_train)
        probas_test_ = model.predict_proba(X_test)
        assert len(probas_test_) == len(X_test)

        if mean:
            collect_probas_test += list(probas_test_[:, 1])
            collect_labels_test += list(y_test)
        else:
            collect_auc_test += [roc_and_auc(probas_test_[:, 1], y_test, ax_test, title=title)]

        if evaluate_train:
            probas_train_ = model.predict_proba(X_train)
            assert len(probas_train_) == len(X_train)
            if mean:
                collect_probas_train += list(probas_train_[:, 1])
                collect_labels_train += list(y_train)
            else:
                collect_auc_train += [roc_and_auc(probas_train_[:, 1], y_train, ax_train, title=title)]

    if mean:
        auc_test = roc_and_auc(collect_probas_test, collect_labels_test, ax_test, title=title)
        if ax_test is not None:
            ax_test.set_title('test set ROC' % auc_test)
        if evaluate_train:
            auc_train = roc_and_auc(collect_probas_train, collect_labels_train, ax_train, title=title)
            if ax_train is not None:
                ax_train.set_title('train set ROC' % auc_train)
            return auc_test, auc_train
        return auc_test

    else :
        if ax_test is not None:
            ax_test.set_title('test set ROC')
        if evaluate_train:
            if ax_train is not None:
                ax_train.set_title('train set ROC')
            return collect_auc_test, collect_auc_train
        else:
            return collect_auc_test


"""
        if evaluate_train:
            probas_train_ = model.predict_proba(X_train)
            fpr, tpr, _ = roc_curve(y_train, probas_train_[:, 1])
            if mean:
                collect_auc_train += [auc(fpr, tpr)]


            if evaluate_train:
                collect_probas_train += probas_train_[:, 1]
                collect_labels_test += y_train

        else:
            fpr, tpr, _ = roc_curve(y_test, probas_test_[:, 1])
            collect_fpr_tpr_test += [(fpr, tpr)]
            if evaluate_train:
                fpr, tpr, _ = roc_curve(y_train, probas_train_[:, 1])
                collect_fpr_tpr_train += [(fpr, tpr)]

    if mean:


    if ax is not None:
        if evaluate_train:
            ax_test = ax[0]
            ax_train = ax[1]
            if mean:

                ax.plot(fpr, tpr)

    if mean:
        mean_auc_test =  sum(collect_auc_test)/len(collect_auc_test)
        if evaluate_train:
            mean_auc_train =  sum(collect_auc_train)/len(collect_auc_train)
            return mean_auc_test, mean_auc_train
        else:
            return mean_auc_test
    else:
"""
