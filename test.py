
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model, datasets
import vowpal_wabbit_wrapper as vw


X = np.r_[np.random.randn(20, 2) - [0.5, 0.5], np.random.randn(20, 2) + [0.5,0.5]]
Y = [0] * 20 + [1] * 20

h = .02  # step size in the mesh

logreg = linear_model.LogisticRegression(C=1e5)


# we create an instance of Neighbours Classifier and fit the data.
logreg.fit(X, Y)


# Plot the decision boundary. For that, we will assign a color to each
# point in the mesh [x_min, m_max]x[y_min, y_max].
x_min, x_max = X[:, 0].min() - 2, X[:, 0].max() + 2
y_min, y_max = X[:, 1].min() - 2, X[:, 1].max() + 2
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))


Z = np.array([int(p < 0.5) for p,q in logreg.predict_proba(np.c_[xx.ravel(), yy.ravel()])])

#print Z

logreg = vw.VW(name="logistic_test2", bfgs=True, passes=200, extra_options=['--quiet'])
# we create an instance of Neighbours Classifier and fit the data.
logreg.fit(X, Y)
Z = np.array([int(p < 0.5) for p,q in logreg.predict_proba(np.c_[xx.ravel(), yy.ravel()])])

#print logreg.predict_proba(X).tostring()

print Z



# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure(1, figsize=(4, 3))
plt.pcolormesh(xx, yy, Z, cmap=plt.cm.Paired)

# Plot also the training points
plt.scatter(X[:, 0], X[:, 1], c=Y, edgecolors='k', cmap=plt.cm.Paired)

plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.xticks(())
plt.yticks(())

plt.show()