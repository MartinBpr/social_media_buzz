import data_handling as dh
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.cross_validation import train_test_split
import metrics
from tools import tic, toc
import numpy as np
import pandas as pd
from data_handling import first_difference
from vowpal_wabbit_wrapper import VW

recreate_data = False
#limit = 70000
limit = None
no_scale = True
n_buckets = None
first_difference = False

dismissed_features = []
#dismissed_features = ['CS','ADL', 'BL']


store_file = 'cache_data/data_and_labels'
if limit : 
    store_file += '_l' + str(limit)
if n_buckets :
    store_file += '_nb' + str(n_buckets)

if first_difference :
    store_file += '_fd'
    
if len(dismissed_features) > 0 :
    store_file += '_dismissed' + '_'.join(dismissed_features)

store_file += '.npz'
tic()

try :
    assert not recreate_data 
    stored_data = np.load(store_file)
    print 'Data found in cache'

    X =  stored_data['arr_0']
    y =  stored_data['arr_1']

except (AssertionError,IOError), e:
    print 'Recreating data'
    data, labels = dh.import_data(limit = limit)
    #print dh.describe_data(data)
    for feature_name in dismissed_features :
        for f in dh.features_names_dict[feature_name] :
            data = data.drop(f, axis = 1)
            
            
    if first_difference :
        for f in dh.features :
            data = dh.first_difference(data, dh.features_names_dict[f], replace = True)

    if n_buckets is None :
        if not no_scale :
            data, _ = dh.scale_features(data, style = 'standard')
    else : 
        data, _ = dh.bucket_features(data, dh.all_features(), n_buckets=n_buckets, replace=True)
    #print data.describe()
    #print dh.describe_data(data)

    #print labels.describe()
    #print dh.all_features()
    X = data.values
    y = labels.values
    np.savez(store_file, X, y)
toc()

x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.33)

print ''
print 'Training model'

#model = LogisticRegression(penalty='l2', dual=False, tol=0.000001, C=0.1, fit_intercept=True, intercept_scaling=1, class_weight=None, random_state=None)
#model = nn.NeuralNetwork(hidden_units = 20, maxEpochs = 2)
#model = svm.SVC(C=0.01, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, shrinking=True, probability=True, tol=0.001, cache_size=200, class_weight=None, verbose=True, max_iter=-1, random_state=None)
model =  RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=5)

tic()
model.fit(x_train, y_train)
toc()

print ''
print 'Testing model'
tic()
ax1 = plt.subplot(221)
metrics.ROC_Curve(model, x_test, y_test, ax = ax1, n_bootstrap = 10)
#ax1.set_title('Test set')


ax2 = plt.subplot(222)
metrics.PR_Curve(model, x_test, y_test, ax = ax2, n_bootstrap = 10)


ax3 = plt.subplot(223)
metrics.ROC_Curve(model, x_train, y_train, ax = ax3, n_bootstrap = 10)
#ax3.set_title('Train set')


ax4 = plt.subplot(224)
metrics.PR_Curve(model, x_train, y_train, ax = ax4, n_bootstrap = 10)

toc()
plt.show()


"""
# Etude des features

l = model.feature_importances_
print sorted(l)
sorted_index = sorted(range(len(l)), key=lambda k: l[k])

dh.import_data(1)
f_names = dh.all_features()

classed_features = [f_names[s] for s in sorted_index]
classed_features.reverse()
print classed_features
"""

