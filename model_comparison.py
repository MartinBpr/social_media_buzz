import metrics
import vowpal_wabbit_wrapper as vw
import data_handling as dh
import matplotlib.pyplot as plt
import string
from sklearn import cross_validation
from sklearn.metrics import roc_curve, auc
from sklearn.ensemble import RandomForestClassifier
from model_aggregator import ModelAggregator

limit = None
data, labels = dh.import_data(limit=limit)

data = dh.drop_noise_features(data)

data, groups, feature_names, feature_splits_and_names, ignore_letter_mapper = dh.create_name_spaces_for_vw(data, feature_group=1)

print ignore_letter_mapper
print feature_splits_and_names

kf = cross_validation.KFold(len(data), n_folds=5, shuffle=True, random_state=0)


ax1 = plt.subplot(121)
ax2 = plt.subplot(122)

X = data.values
y = labels.values


### logistic

model_logistic = vw.VW(name="logistic_fs2", loss='logistic', passes=20, extra_options=['--quiet'])
model_logistic.feature_splits_and_names = feature_splits_and_names
model_logistic.feature_names = feature_names

metrics.cross_fold_roc(model_logistic, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'logistic')

### Random forrest

model_rf = RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=8)
metrics.cross_fold_roc(model_rf, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'RF, depth 8')

### neural network
model_nn = vw.VW(name="logistic_fs2", nn=15, passes=20, extra_options=['--quiet'])#, quadratic='aa')
model_nn.feature_splits_and_names = feature_splits_and_names
model_nn.feature_names = feature_names
metrics.cross_fold_roc(model_nn, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'NN 15')

plt.show()

### Aggregation

#model_aggregated = ModelAggregator([model_logistic, model_rf, model_nn])
#model_aggregated.set_features_names_and_split(feature_names, feature_splits_and_names)
#metrics.cross_fold_roc(model_aggregated, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'Aggregation uniform')

#model_aggregated = ModelAggregator([model_logistic, model_rf, model_nn], weights=[0.3, 0.6, 0.1])
#metrics.cross_fold_roc(model_aggregated, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'Aggregation weighted')

#plt.show()


### test aggregation with several features sources

#def init_to_keep(model, list_to_keep):
#    for f in list_to_keep:
#        model.extra_options.append('--keep %s' % ignore_letter_mapper[f])
#    model.feature_splits_and_names = feature_splits_and_names
#    model.feature_names = feature_names

#model_nn1 = vw.VW(name="logistic_fs2", nn=15, passes=20, extra_options=['--quiet'])
#init_to_keep(model_nn1, ['NA', 'AI'])

#model_nn2 = vw.VW(name="logistic_fs2", nn=15, passes=20, extra_options=['--quiet'])
#init_to_keep(model_nn2, ['ADL', 'NAC', 'NCD', 'NAD'])

#model_nn3 = vw.VW(name="logistic_fs2", nn=15, passes=20, extra_options=['--quiet'])
#init_to_keep(model_nn3, ['AS(NA)', 'AS(NAC)'])

#model_aggregated = ModelAggregator([model_nn2, model_nn3])
#metrics.cross_fold_roc(model_nn1, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'NA, AI')
#metrics.cross_fold_roc(model_nn2, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'ADL, NAC, NCD, NAD')
#metrics.cross_fold_roc(model_nn3, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'AS(NA), AS(NAC)')
#metrics.cross_fold_roc(model_aggregated, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'Aggregation')


#plt.show()

### Choose RF depth

#model_rf = RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=6)
#metrics.cross_fold_roc(model_rf, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'RF, depth 6')

#model_rf = RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=7)
#metrics.cross_fold_roc(model_rf, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'RF, depth 7')

#model_rf = RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=8)
#metrics.cross_fold_roc(model_rf, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'RF, depth 8')

#model_rf = RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=9)
#metrics.cross_fold_roc(model_rf, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'RF, depth 9')

#model_rf = RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=10)
#metrics.cross_fold_roc(model_rf, X, y, kf=kf, ax=[ax1, ax2], mean=True, evaluate_train=True, title = 'RF, depth 10')
