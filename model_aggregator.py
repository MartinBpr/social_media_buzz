import numpy as np
from vowpal_wabbit_wrapper import VW

class ModelAggregator:
    def __init__(self, model_list, weights=None):
        self.model_list = model_list
        self.n_models = len(model_list)

        if weights is not None:
            assert len(weights) == self.n_models
            self.weights = np.array(weights)/sum(weights)
        else:
            self.weights = np.ones(self.n_models) / self.n_models

    def set_features_names_and_split(self, feature_names, feature_splits_and_names):
        for model in self.model_list:
            if model.__class__ == VW().__class__:
                model.feature_splits_and_names = feature_splits_and_names
                model.feature_names = feature_names

    def fit(self, X, y):
        for model in self.model_list:
            model.fit(X,y)

    def predict_proba(self, X):
        predicted_probas = None
        for i,model in enumerate(self.model_list):
            pp = model.predict_proba(X)

            if predicted_probas is None:
                predicted_probas = pp * self.weights[i]
            else:
                predicted_probas += pp * self.weights[i]
        return predicted_probas


