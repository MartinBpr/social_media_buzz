﻿Martin Bompaire, Anna Korba
---------------------------

* mail: martin.bompaire@ensae.fr;anna.korba@ensae.fr
* données: http://ama.liglab.fr/resourcestools/datasets/buzz-prediction-in-social-media/
* bitbucket: https://bitbucket.org/MartinBpr/social_media_buzz/src/fc3396fa6c479c43944e96f9d235fd0f629f3372?at=master
* gitlab: https://versioning.ensae.fr/python-2a/bompaire-korta.git
* video: https://www.youtube.com/watch?v=DGLtdCS7vGA

??/11
+++++

Pour le projet de python avancé, nous avons trouvé un sujet qui nous semble intéressant:
http://ama.liglab.fr/resourcestools/datasets/buzz-prediction-in-social-media/

Il s'agit de détecter quels tweets seront des "buzz" en étudiant des fenêtres de sept jours où la popularité du tweet est mesurée. Le but est donc de faire une classification sur ces données et de prédire si ces tweets seront des buzzs ou non. La base de données est la suivante:
https://archive.ics.uci.edu/ml/datasets/Buzz+in+social+media+
	

Il y a près de 140 000 données et 11 features (par jour, ce qui en fait 77 sur une fenêtre de six jours).

17/11
+++++

* Projet bien avancé, deux modèles, régression logistique + réseaux de neurones (modèles utilisés inspirés de vos stages respectifs ?)
* Rappeler certaines définitions (que veut dire True Positif sur vos graphes, comparer plusieurs modèles sur la même courbe)
* Investir les features ou les modèles ? --> regarder les erreurs devraient vous aider à choisir
* Etablir une liste des idées à essayer et se concentrer sur celles-ci plutôt que de tout essayer.
* Autres directions : quelques idées que vous avez eues pendant votre stage et que vous n'avez pas eu de réaliser (si toutefois c'est applicable dans ce cas)
* Dans ce projet, ce qui m'intéresse plus, c'est le raisonnement ou l'intuition qui vous incite à essayer telle idée, telle feature, modèle plutôt qu'un autre, moins la performance des modèles même si elle a une incidence sur vos choix.
* Dans le même esprit, je reviens sur la courbe ROC qui avait une allure inhabituelle, c'est déjà bien de le remarquer, je ne dis pas qu'il faut prendre du temps pour découvrir pourquoi mais ce sont parfois ces choses inattendues qui débouchent sur de nouvelles idées, une particularité des données qu'on peut parfois exploiter à son avantage…
* Quelques notes rédigées à partir des deux suivis d'aujourd'hui : http://www.xavierdupre.fr/app/ensae_teaching_cs/helpsphinx3/questions/question_2014_project.html#pourquoi-la-regression-logistique-marche-bien-lorsqu-une-classe-est-sous-representee.


rapport
-------

- excellent rapport, je reste un peu sur ma fin en constatant les différences entre librairies
  (les taux d'erreur ne sont pas indiqués, seules les AUC le sont mais l'AUC n'est pas toujours facile
  à interpréter en terme de performance, d'autant plus que durant la vidéo, vous affirmez que seule une partie de la courbe
  est intéressante alors que l'AUC prend en compte toute la courbe

    - Nous n'avons pas renseigné le taux d'erreur car nous n'avons pas voulu fixer de seuil décisionnel. En voulant minimiser
      le taux d'erreur nous n'aurions retrouvé quasi aucun buzz. Quant à la courbe nous voulions préciser ce que
      la random forest avait en plus, et qui se voyait sur le coin inférieur gauche de la courbe.

- la classe buzz est sous-représenté, il faudrait donner son poids et s'assurer que les classifieurs
  proposé ne classent pas toutes les observations dans la même classes

    - Il faut ajuster le seuil décisionnel en fonction du problème pour avoir le bon compromis précision - rappel.

- j'aurais aimé comprendre un peu mieux les différences entre les expériences réalisées sur les deux librairies
  (paramètres, nombre dobservations, ...), vous ne proposez pas d'hypothèses même si c'est pour dire ensuite que cela
  aurait demandé trop de temps à vérifier

    - Les différences entre libraries sont probablement dues à un preprossessing interne à vowpal wabbit
      mais nous n'avons pas eu le courage de le décortiquer. En ce qui concerne le nombre d'observations, nous les
      avons systématiquement toutes utilisées.

- la description du problème et des variables est très complète
- certains résultats auraient pu être interprétés (voir commentaire sur la vidéo)
- seule ombre au tableau, les perspectives : que peut-on fait avec ce modèle ?, de votre conclusion, on en déduit que vous n'avez
  pas fait l'expérience qui vous aurait permis de conclure sur ce point ? C'est-à-dire, entraîner sur le passé et vérifier
  l'évolution du modèle sur un passé plus proche, pas d'intuition à partager sur le sujet ?

    - Nous n'avions pas les timestamps de nos observations pour mener à bien notre expérience. D'intuition les features semblent
      pouvoir tenir le choc car elles devraient bien s'adapter à de nouveaux sujets.

- vous dites que les namespaces ont aidé mais vous ne dites pas exactement en quoi

    - Nous avons pu nous en servir pour ignorer nos variables par groupe (pour la sélection de feature) mais aussi pour croiser nos variables, sans grand succès.

code
----

- joli boulot au niveau du wrapper, il faudrait peut être songer à le partager

    - Si nous trouvons le temps de le compléter et de le stabiliser pourquoi pas !

- code plutôt clair et bien séparé

video
-----

- la vidéo est très claire
- l'utilisation de l'encre rouge sur l'écran qui apparaît au fur et à mesure du commentaire est une très bonne idée
  (en particulier sur la courbe ROC)
- vous donnez la liste des variables et jours supprimés ou gardés (sélection de variables) mais vous ne donnez pas d'interprétation
  ni dans la vidéo, ni dans le rapport
- conclusion de votre vidéo : les features font tout le boulot, le classifieur a moins d'importance, avez-vous mesurer l'AUC obtenue pour une seule feature ?




