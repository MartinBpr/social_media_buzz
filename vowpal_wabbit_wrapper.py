import subprocess
import numpy as np
import os
import data_handling as dh
import itertools
import matplotlib.pyplot as plt

import logging
FORMAT = 'vw : %(message)s'
logging.basicConfig(format=FORMAT,level=logging.DEBUG)
logger = logging.getLogger('tcpserver')


class VW:

    def __init__(self, name="no_name", extra_options=[], extra_train_options=[], chosen_path=None,
                 bits=None, silent=False,
                 loss=None, passes=1, quadratic=None,
                 l1=None, l2=None, learning_rate=None,
                 power_t=None, adaptive=False, decay_learning_rate=None, initial_t=None,
                 minibatch=None, bfgs=None,
                 nn=None):

        self.name = name
        self.extra_options = extra_options
        self.extra_train_options = extra_train_options

        vw_path = '/Users/Martin/Projects/vowpal_wabbit/vowpalwabbit/'
        self.vw = vw_path + "vw"

        if not chosen_path:
            self.path = self.set_and_create_folder(os.getcwd() + "/vw")
        else:
            self.path = self.set_and_create_folder(chosen_path)

        # will be defined at creation to add hash value
        self.model_path = None
        self.predict_path = None
        self.train_data_path = None
        self.test_data_path = None

        self.feature_names = None
        self.feature_splits_and_names = None

        self.name = name
        self.bits = bits
        self.loss = loss
        self.l1 = l1
        self.l2 = l2
        self.learning_rate = learning_rate
        self.silent = silent
        self.passes = passes
        self.quadratic = quadratic
        self.power_t = power_t
        self.adaptive = adaptive
        self.decay_learning_rate = decay_learning_rate
        self.initial_t = initial_t
        self.minibatch = minibatch
        self.bfgs = bfgs
        self.nn = nn

    @staticmethod
    def set_and_create_folder(path):
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def _sk_data_to_vw_file(self, X, Y=[], feature_names=None, feature_splits_and_names=None, class_weighting=False):
        """
        X : np array for features
        Y : np array for labels
        feature_names : list of names for the features in X
        feature_splits_and_names : dict with key split_position and value split_name
        class_weighting : using of vw automatic observation weighting
        :rtype : None
        """
        if feature_names is None:
            feature_names = self.feature_names
        if feature_splits_and_names is None:
            feature_splits_and_names = self.feature_splits_and_names

        if isinstance(X, list):
            X = np.array(X)
        if isinstance(Y, list):
            Y = np.array(Y)

        # We hash values in order to know if we should recreate data

        contains_label = len(Y) != 0
        if contains_label:
            hash_value = hash(np.concatenate((X, np.array([Y]).T), axis=1).tostring() +
                str(feature_names) + str(feature_splits_and_names) + str(class_weighting))

            data_path = self.set_and_create_folder(self.path + '/train_data/') + self.name + \
                '#' + str(abs(hash_value)) + '#' + ".data"
            self.train_data_path = data_path
        else:
            hash_value = hash(X.tostring() +
                str(feature_names) + str(feature_splits_and_names) + str(class_weighting))

            data_path = self.set_and_create_folder(self.path + '/test_data/') + self.name + \
                '#' + str(abs(hash_value)) + '#' + ".data"
            self.test_data_path = data_path

        if os.path.exists(data_path):
            logger.info('data found in cache at %s' % data_path)
            return

        # few assertions about dimensions
        global weight
        n, d = X.shape
        if contains_label:
            assert n == len(Y)
        if feature_names is None:
            feature_names = ['f%i' % i for i in range(d)]
        assert len(feature_names) == d

        # we only over sample class 1
        if class_weighting:
            ratio = float(np.count_nonzero(Y))/n
            assert ratio < 1
            weight = 0.5/ratio

        data_file = open(data_path, 'w')
        for (x, y) in itertools.izip_longest(X, Y):
            line_vw = ''

            if contains_label:
                label = y
                # we only do binary classification
                assert y in [0, 1]

                # convert to -1, +1 labels
                line_vw += '%i' % int(2*label - 1)

                if class_weighting and label == 1:
                    line_vw += " %.2f" % weight
            else :
                line_vw += '%i' % (np.random.randint(2)*2 - 1)
            for i, f in enumerate(feature_names) :
                if feature_splits_and_names and i in feature_splits_and_names.keys():
                    line_vw += " |%s " % feature_splits_and_names[i]
                elif i == 0:
                    line_vw += " |"
                if x[i] != 0:
                    line_vw += "%s:%.4g " % (f, x[i])# f + ':' + str(x[i])
            line_vw += '\n'
            data_file.write(line_vw)

    #X and Y are numpy arrays as for scikit learn
    def fit(self, X, Y, feature_names=None, feature_splits_and_names=None, class_weighting=False):

        self._sk_data_to_vw_file(X, Y=Y, feature_names=feature_names,
                                 feature_splits_and_names=feature_splits_and_names, class_weighting=class_weighting)

        # command line that will be executed
        train_cl = self.vw
        train_cl += ' -d ' + self.train_data_path


        if self.quadratic == 'all':
            if feature_splits_and_names is None:
                feature_splits_and_names = self.feature_splits_and_names
            assert feature_splits_and_names is not None
            quadratic = ''
            for i in itertools.combinations(feature_splits_and_names.values(), 2):
                quadratic += ' -q %s' % ''.join(i)

        l = list()
        l.append('--passes %d' % self.passes)
        if self.bits                is not None: l.append('-b %d' % self.bits)
        if self.learning_rate       is not None: l.append('--learning_rate=%f' % self.learning_rate)
        if self.l1                  is not None: l.append('--l1=%g' % self.l1)
        if self.l2                  is not None: l.append('--l2=%g' % self.l2)
        if self.initial_t           is not None: l.append('--initial_t=%f' % self.initial_t)
        if self.quadratic           == 'all': l.append('%s' % quadratic)
        if self.power_t             is not None: l.append('--power_t=%f' % self.power_t)
        if self.loss                is not None: l.append('--loss_function=%s' % self.loss)
        if self.decay_learning_rate is not None: l.append('--decay_learning_rate=%f' % self.decay_learning_rate)
        if self.bfgs:                            l.append('--bfgs')
        if self.adaptive:                        l.append('--adaptive')
        if self.nn                  is not None: l.append('--nn %d' % self.nn)

        train_cl += ' ' + ' '.join(l)

        train_cl += ' ' + ' '.join(self.extra_train_options)
        train_cl += ' ' + ' '.join(self.extra_options)

        # ask for a cache file if we do more than one pass
        if '--passes ' in train_cl and '--passes 1 ' not in train_cl:
            train_cl += ' -c'

        hash_value = hash(train_cl)
        self.model_path = self.set_and_create_folder(self.path + '/model/') + self.name + '#' + \
            str(abs(hash_value)) + "#.model"

        if os.path.exists(self.model_path):
            logger.info('model found in cache %i' % hash_value)
        else:
            train_cl += ' -f ' + self.model_path
            logger.info('executing "%s"' % train_cl)
            subprocess.call(train_cl, shell=True)

            # sometimes something goes wrong and we must retry
            if not os.path.exists(self.model_path):
                logger.info('model not saved, trying again %s' % self.predict_path)
                self.fit(X, Y, feature_names=feature_names, feature_splits_and_names=feature_splits_and_names,
                         class_weighting=class_weighting)

    def predict_proba(self, X, feature_names=None, feature_splits_and_names=None):
        if isinstance(X, list):
            X = np.array(X)

        if not self.model_path:
            raise(BaseException('Model not fitted yet'))
        else:
            # command line that will be executed
            self._sk_data_to_vw_file(X, feature_names=feature_names, feature_splits_and_names=feature_splits_and_names)

            test_cl = self.vw
            test_cl += ' -d ' + self.test_data_path
            test_cl += ' -i ' + self.model_path
            test_cl += ' -t '
            if self.quadratic == 'all':
                if feature_splits_and_names is None:
                    feature_splits_and_names = self.feature_splits_and_names
                assert feature_splits_and_names is not None
                quadratic = ''
                for i in itertools.combinations(feature_splits_and_names.values(), 2):
                    quadratic += ' -q %s' % ''.join(i)
            if self.quadratic == 'all':
                test_cl += '%s' % quadratic

            hash_value = hash(self.model_path + self.test_data_path)
            self.predict_path = self.set_and_create_folder(self.path + '/predict/') + self.name + '#' + \
                str(abs(hash_value)) + "#.predict"

            if os.path.exists(self.predict_path):
                logger.info('prediction found in cache %s' % self.predict_path)
            else:
                test_cl += ' -p ' + self.predict_path
                test_cl += ' ' + ' '.join(self.extra_options)
                logger.info('executing "%s"' % test_cl)
                subprocess.call(test_cl, shell=True)

            pred_file = open(self.predict_path, 'r')

            predict_proba = []
            for l in pred_file.readlines():
                value = float(l.strip())
                if self.nn is not None:
                    linked_value = value/2 + 0.5
                else:
                    linked_value = 1.0/(1 + np.exp(-value))
                predict_proba += [[1-linked_value, linked_value]]

            # sometimes prediction is not totally done
            if len(predict_proba) != len(X):
                os.remove(self.predict_path)
                logger.info('removed file %s, trying again' % self.predict_path)
                self.predict_proba(X, feature_names, feature_splits_and_names)

            predict_proba = np.array(predict_proba)
            return predict_proba


#subprocess.call('source ~/.bash_profile', shell=True)
#subprocess.call('/bin/bash -l -c "ll"', shell=True)
#subprocess.call('ll', shell=True, executable='/bin/bash')

"""
limit = None
data, labels = dh.import_data(limit=limit)

lambda_l1 = 0.1/len(data)
print lambda_l1

model = VW(name="logistic", loss='logistic', passes=9, extra_options=['--quiet'])
#model = VW(name="logistic", nn=5, passes=9)#, extra_options=['--quiet'])

model.feature_names = data.columns

features_prefix = [c[:-2] for c in data.columns if "_0" in c]

#model.feature_splits_and_names = {}
#for i,f in enumerate(features_prefix):
#    model.feature_splits_and_names[i*7] = f

X = data.values
y = labels.values

from sklearn import cross_validation
from sklearn.metrics import roc_curve, auc

kf = cross_validation.KFold(len(X), n_folds=5, shuffle=True, random_state=0)

ax1 = plt.subplot(121)
ax2 = plt.subplot(122)

roc_auc_list_test = []
roc_auc_list_train = []
for i, (train_index, test_index) in enumerate(kf):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    model.fit(X_train, y_train, class_weighting=True)
    probas_test_ = model.predict_proba(X_test)
    probas_train_ = model.predict_proba(X_train)

    fpr, tpr, _ = roc_curve(y_test, probas_test_[:, 1])

    roc_auc_list_test += [auc(fpr, tpr)]
    ax1.plot(fpr, tpr)

    fpr, tpr, _ = roc_curve(y_train, probas_train_[:, 1])
    roc_auc_list_train += [auc(fpr, tpr)]
    ax2.plot(fpr, tpr, label='fold %i' % i)

mean_auc_test =  sum(roc_auc_list_test)/len(roc_auc_list_test)
ax1.set_title('test : ROC curve (area = %0.4f)' % mean_auc_test)

mean_auc_train =  sum(roc_auc_list_train)/len(roc_auc_list_train)
ax2.set_title('train : ROC curve (area = %0.4f)' % mean_auc_train)

plt.legend()
plt.show()

#model.fit(data.values, labels.values, class_weighting=False, feature_names=dh.all_features())
#print model.predict_proba(data.values)
"""