import metrics
import vowpal_wabbit_wrapper as vw
import data_handling as dh
import matplotlib.pyplot as plt
import string
from sklearn import cross_validation
from sklearn.metrics import roc_curve, auc
from sklearn.ensemble import RandomForestClassifier
from model_aggregator import ModelAggregator
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression


### We compare the biggest errors made by our models
### Do they make this mistakes on the same observations?

limit = None
data, labels = dh.import_data(limit=limit)

data = dh.drop_noise_features(data)

data, groups, feature_names, feature_splits_and_names, ignore_letter_mapper = dh.create_name_spaces_for_vw(data, feature_group=1)

X = data.values
y = labels.values

### logistic VW

model_logistic = vw.VW(name="compare_errors", loss='logistic', passes=20, extra_options=['--quiet'])
model_logistic.feature_splits_and_names = feature_splits_and_names
model_logistic.feature_names = feature_names

### logistic sklearn
model_logistic_sk = LogisticRegression(C=1e10) # for no penalization

### Random forrest

model_rf = RandomForestClassifier(n_estimators=50,oob_score=True,max_depth=8)

### neural network
model_nn = vw.VW(name="compare_errors", nn=15, passes=20, extra_options=['--quiet'])#, quadratic='aa')
model_nn.feature_splits_and_names = feature_splits_and_names
model_nn.feature_names = feature_names

x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)

models = [model_logistic, model_logistic_sk, model_rf, model_nn]
predicted_probas = []
errors = []
for model in models:
    model.fit(x_train, y_train)
    pp = model.predict_proba(x_test)
    predicted_probas += [pp]
    # 1-score if label is 1, score if label is 0
    errors += [y_test + (1 - 2*y_test)*pp[:, 1]]

#index = 3#261
#print y_test[index]
#print [pp[index] for pp in predicted_probas]
#print [e[index] for e in errors]

sorted_index_errors = []
for e in errors:
    current_index_errors = []
    for index, error in enumerate(e):
        current_index_errors += [[index, error]]
    current_index_errors.sort(key= lambda x: x[1])
    current_index_errors.reverse()
    sorted_index_errors += [current_index_errors]

#print sorted_index_errors

selected_indexes = 1000
worst_indexes = [[i for i, e in s_i_e[:selected_indexes]] for s_i_e in sorted_index_errors ]

print 'l vw, l sk', float(len(set(worst_indexes[0]).intersection(set(worst_indexes[1]))))/selected_indexes
print 'l vw, rf sk', float(len(set(worst_indexes[0]).intersection(set(worst_indexes[2]))))/selected_indexes
print 'l vw, nn vw', float(len(set(worst_indexes[0]).intersection(set(worst_indexes[3]))))/selected_indexes
print 'l sk, rf dk', float(len(set(worst_indexes[1]).intersection(set(worst_indexes[2]))))/selected_indexes
print 'l sk, nn vw', float(len(set(worst_indexes[1]).intersection(set(worst_indexes[3]))))/selected_indexes
print 'rf sk, nn vw', float(len(set(worst_indexes[2]).intersection(set(worst_indexes[3]))))/selected_indexes
