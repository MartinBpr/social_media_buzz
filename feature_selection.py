import metrics
import vowpal_wabbit_wrapper as vw
import data_handling as dh
import matplotlib.pyplot as plt
import string
from sklearn import cross_validation
from sklearn.metrics import roc_curve, auc

limit = None
data, labels = dh.import_data(limit=limit)

feature_group = 2
data, groups, feature_names, feature_splits_and_names, ignore_letter_mapper = dh.create_name_spaces_for_vw(data, feature_group=feature_group)

# we add few more custom feature to remove
# '' is to keep all features
if feature_group == 1:
    '''groups +'''
    groups = ['nothing removed'] + groups + [['CS', 'BL'], ['CS', 'BL', 'AS(NA)'], ['CS', 'BL', 'ADL'], ['CS', 'BL', 'AS(NAC)'], ['CS', 'BL', 'AT'], ['CS', 'BL', 'AT', 'ADL'], ['AS(NAC)', 'BL', 'CS', 'AS(NA)']]
elif feature_group == 2:
    groups = ['nothing removed'] + groups + [['_0', '_1']] + [['_0', '_1', '_2']]

#groups = ['']#['CS', 'BL', 'AT']]

kf = cross_validation.KFold(len(data), n_folds=5, shuffle=True, random_state=0)

# to store auc
fp_train_auc = {}
fp_test_auc = {}

for fp in groups:#[0:1]:
    print 'ignoring', fp

    model = vw.VW(name="logistic_fs2", loss='logistic', passes=9, extra_options=['--quiet'])
    #model = vw.VW(name="logistic", nn=5, passes=9)#, extra_options=['--quiet'])

    model.feature_names = feature_names
    model.feature_splits_and_names = feature_splits_and_names

    if fp.__class__ == [].__class__:
        for f in fp:
            model.extra_options.append('--ignore %s' % ignore_letter_mapper[f])
    elif fp != 'nothing removed':
        model.extra_options.append('--ignore %s' % ignore_letter_mapper[fp])

    X = data.values
    y = labels.values



    roc_auc_list_test = []
    roc_auc_list_train = []
    for i, (train_index, test_index) in enumerate(kf):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        model.fit(X_train, y_train)
        probas_test_ = model.predict_proba(X_test)
        probas_train_ = model.predict_proba(X_train)

        fpr, tpr, _ = roc_curve(y_test, probas_test_[:, 1])
        roc_auc_list_test += [auc(fpr, tpr)]

        fpr, tpr, _ = roc_curve(y_train, probas_train_[:, 1])
        roc_auc_list_train += [auc(fpr, tpr)]

    mean_auc_test =  sum(roc_auc_list_test)/len(roc_auc_list_test)
    fp_test_auc[str(fp)] = mean_auc_test

    mean_auc_train =  sum(roc_auc_list_train)/len(roc_auc_list_train)
    fp_train_auc[str(fp)] = mean_auc_train

print fp_test_auc
print fp_train_auc

fig = plt.figure()

if feature_group == 1:
    fig.suptitle('AUC after variables removal', fontsize=20)
elif feature_group == 2:
    fig.suptitle('AUC after day removal', fontsize=20)

ax1 = fig.add_subplot(1,2,1)
metrics.plot_dict_bar(fp_test_auc, ax1, groups)
ax1.set_title('On test set')

ax2 = fig.add_subplot(1,2,2)
metrics.plot_dict_bar(fp_train_auc, ax2, groups)
ax2.set_title('On train set')

plt.show()
