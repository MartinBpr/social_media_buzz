from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np
import data_handling as dh
import pandas as pd

### We do a PCA on our last day features

X_all,Y = dh.import_data(10000)

X_all = dh.drop_noise_features(X_all, ['CS', 'BL', 'ADL', 'AT'])

X_all = dh.scale_feature(X_all, rtype='pandas')

last_day_features = [column for column in X_all.columns if '_6' in column]
X = X_all[last_day_features]

assert X.__class__ == pd.DataFrame().__class__

print 'data scaled'

x_transpose = X.T
pca = PCA(n_components=2)
pca.fit(x_transpose)

ax1 = plt.subplot(121)
ax1.bar(np.arange(len(pca.explained_variance_ratio_))+0.5, pca.explained_variance_ratio_)
plt.title("Variance expliquee")


import warnings
warnings.filterwarnings('ignore')
X_reduced = pca.transform(X.T)

ax2 = plt.subplot(122)
ax2.scatter(X_reduced[:, 0], X_reduced[:, 1])

X_actives_reduced = pca.transform(X.T)
for label, x, y in zip(x_transpose.index, X_reduced[:, 0], X_reduced[:, 1]):
    plt.annotate(
        label,
        xy = (x, y), xytext = (-20, 20),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

plt.show()