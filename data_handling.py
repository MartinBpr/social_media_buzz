import pandas as pd
from sklearn import preprocessing
import numpy as np
import string

features = ['NCD', 'AI', 'AS(NA)', 'BL', 'NAC', 'AS(NAC)', 'CS', 'AT', 'NA', 'ADL', 'NAD']
features_names_dict = {}

def all_features():
    return reduce(lambda x,y:x+y,features_names_dict.values())

def import_data(limit = None) :
    
    if not limit :
        limit = 1000000000
    
    df = pd.read_csv('data/Twitter-Relative-Sigma-500.data', header = None, nrows = limit)
    
    columns = []
    for f in features :
        features_names_dict[f] = []
        for i in range(7) :
            f_name = f + '_' + str(i)
            columns += [f_name]
            features_names_dict[f] += [f_name]
    columns += 'y'
    
    df.columns = columns
    
    #scikit likes it as float
    for f in features :
        for fn in features_names_dict[f] :
                df[fn] = df[fn].astype(float)
                
    data = df[all_features()]
    labels = df['y']
    return data,labels

def describe_data(df):
    s = ''
    for f in features :
        s += str(df.describe()[features_names_dict[f]])
        s += '\n\n'

    return s

def scale_features(df, style = 'standard', scalers = {}):
    for f in df.columns :
        if f not in scalers :
            scaled_features, scaler = scale_feature(df[f].values, style=style)
            scalers[f] = scaler
            df[f] = scaled_features
        else :
            scaled_features, _ = scale_feature(df[f].values, style=style, scaler = scalers[f])
            df[f] = scaled_features
    return df, scalers
            
def scale_feature(numeric_features, style='standard', scaler=None, rtype='numpy_array'):

    if scaler == None :
        if style == 'minmax':
            scaler = preprocessing.MinMaxScaler()
            scaler.fit(numeric_features)  
        elif style == 'standard':
            scaler = preprocessing.StandardScaler()
            scaler.fit(numeric_features)
        else :
            raise Exception('dunno this style : ' + str(style))

    numeric_features_scaled = scaler.transform(numeric_features)
    if rtype == 'numpy_array':
        return numeric_features_scaled, scaler
    # just to be able to return DataFrame type for PCA
    elif rtype == 'pandas':
        assert numeric_features.__class__ == pd.DataFrame().__class__

        return pd.DataFrame(numeric_features_scaled, columns=numeric_features.columns)


def bucket_features(df, columns, n_buckets = 5, replace = True, levels = None):
    if levels is None :
        levels = []
        if isinstance(n_buckets, list) :
            assert len(columns) == len(n_buckets)
            for i,c in enumerate(columns) :
                df, l = bucket_feature(df, c, n_buckets[i], replace = replace)
                levels += [l]
        else :
            for c in columns :
                df, l = bucket_feature(df, c, n_buckets, replace = replace)
                levels += [l]
    else :
        assert len(levels) == len(columns)
        for c,l in zip(columns,levels) :
            bucket_feature(df, c, n_buckets, replace = replace, levels = l)
                
    return df, levels

def bucket_feature(df, column, n_buckets = 5, replace = True, levels = None):
    assert column.__class__ == ''.__class__
    if levels is None :
        #in case we cannot have that many buckets (for example 0-1 variable)
        while n_buckets >= 1 :
            try :
                buckets, levels =  pd.qcut(df[column], n_buckets, retbins=True)
                #print levels
                break
            except ValueError:
                #print column
                #print e
                n_buckets -= 1
        
        temp_column_name = 'temp'
        #just in case a column is called temp...
        while temp_column_name in df.columns : temp_column_name += '_'
        
        df[temp_column_name] = buckets.labels
        #we do not compute last bucket to avoid colinearity
        for i in range(n_buckets - 1) :
            df[column + '_' +  str(i) + '/' + str(n_buckets - 1)] = df[temp_column_name].apply(lambda x : int(x == i))
            
        df = df.drop(temp_column_name, axis = 1)
        
    else :
        unbounded_levels = list(levels)
        unbounded_levels[0] = -np.Inf
        unbounded_levels[-1] = np.Inf
        
        #we do not compute last bucket to avoid colinearity
        for i in range(len(unbounded_levels) - 2) :
            df[column + '_' +  str(i) + '/' + str(len(unbounded_levels) - 2)] = \
                df[column].apply(lambda x : int(x > unbounded_levels[i] and x <= unbounded_levels[i+1]))
    
    if replace :
            df = df.drop(column, axis = 1)
    return df, levels

'''
# test bucket features
x = np.arange(20)
y = np.arange(10,30)
z = np.arange(10,30)
t = np.arange(15,35)
np.random.shuffle(x)

df = pd.DataFrame(np.array([x,y,z,t]).transpose(), columns=['x','y','z','t'])

df, levels = bucket_features(df, ['x','y'], 5, replace=False)
df, levels = bucket_features(df, ['z','t'], 5, replace=False, levels=levels)

print df
'''

def first_difference(df, columns, replace = False):
    for i in range(len(columns) - 1) :
        c_i = columns[i]
        c_i_plus_1 = columns[i + 1]
        df[c_i_plus_1 + '-' + c_i] = df[c_i_plus_1] - df[c_i]
        
    #we remove columns to avoid colinearity (but we keep the first one)
    if replace :
            df = df.drop(columns[1:], axis = 1)
    return df 

default_removed_features = ['CS', 'BL', 'AT']
def drop_noise_features(df, removed_features=default_removed_features):
    kept_features = list(df.columns)
    for removed_group in removed_features:
        kept_features = [f for f in kept_features if removed_group not in f]
    df = df[kept_features]
    return df


def create_name_spaces_for_vw(data, feature_group=1):
    # 1 for grouping by feature type
    # 2 for grouping by day

    assert feature_group in [1, 2]
    feature_names = data.columns

    if feature_group == 1:
        features_prefix = [c[:-2] for c in feature_names if "_0" in c]
        print 'prefix', features_prefix

    elif feature_group == 2:
        # we sort feature names after reversing the strings to have first all *_0 then *_1 etc.
        feature_names = list(feature_names)
        # str(x)[::-1] reverses the string
        feature_names.sort(key=lambda x: str(x)[::-1])
        # we reorder columns in the data
        data = data[feature_names]

        features_suffix = list(set([c[-2:] for c in feature_names]))
        features_suffix.sort()

    if feature_group == 1:
        groups = features_prefix
    elif feature_group == 2:
        groups = features_suffix

    feature_splits_and_names = {}
    ignore_letter_mapper = {}
    n_groups = len(groups)
    n_features_per_group = len(feature_names)/n_groups

    # hypothesis : groups are equally reparted in feature names
    for i, group_name in enumerate(groups):
        # iteration over abcd...
        name_space_name = string.lowercase[i]
        feature_splits_and_names[i*n_features_per_group] = name_space_name
        # store link between the abcd and the real name of the feature group
        ignore_letter_mapper[group_name] = name_space_name

        # simple verification
        for group_feature in feature_names[i*n_features_per_group:(i+1)*n_features_per_group]:
            assert group_name in group_feature

    return data, groups, feature_names, feature_splits_and_names, ignore_letter_mapper
